package character_management

type Trait struct {
	Name        string
	Description string
}